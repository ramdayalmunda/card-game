const express = require('express');
const app = express()
const PORT = 3005;
const path = require('path');

app.use( express.static( path.join( __dirname, '../client/' ) ) );
app.get('/', ( req, res )=>{ res.sendFile(__dirname, '../client/index.html') })

app.listen( PORT, ()=>{ console.log(`Server http://127.0.0.1:${PORT}`) } );