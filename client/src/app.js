import fullComponent from "./components/full/full.js"
const app = {
    components: {
        fullComponent,
    },
    template : /*html*/`
        <fullComponent ></fullComponent>
    `
}

export default app